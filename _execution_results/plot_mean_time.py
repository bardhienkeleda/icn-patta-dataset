# -*- coding: utf-8 -*-
#!/usr/bin/env python2

import os
from os.path import dirname, abspath
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from statistics import mean
import numpy as np

base_path = (abspath(dirname(__file__)))
logs_path = os.path.join(base_path + '/results_600')

def read_results(results_file):
    df = pd.read_csv(results_file, header=None)
    #get mean accuracy score and mean prediction time
    accuracy_score = df.iat[2, 0].split(':')[-1]
    mean_time = df.iat[1, 0].split(':')[-1]
    #drop these two values and the rest of values
    df.drop(df.head(2).index, inplace=True)
    df.drop(df.head(15).index, inplace=True)
    df.reset_index(drop=True, inplace=True)
    # rename column
    column1 = df[0][0].split(':')[0]
    for row in range(len(df)):
        df[0][row] = df[0][row].split(':')[-1]
    df.columns = [column1]
    # convert from str to float the values and add iterations column
    df['Prediction time per iteration'] = df['Prediction time per iteration'].astype(float)
    predictions_time_list = df['Prediction time per iteration'].tolist()
    #print(df)
    return df, accuracy_score, mean_time, predictions_time_list
if __name__ == '__main__':
    res_file1 = open(logs_path + "/" + "svm_1_1642_results.txt", "r")
    res_file2 = open(logs_path + "/" + "svm_1and2_3004_results.txt", "r")
    dataframe1, acc_score_1, mean_time_1, predictions_time_list1  = read_results(res_file1)
    dataframe2, acc_score_2, mean_time_2, predictions_time_list2 = read_results(res_file2)
    pred_time_list = [predictions_time_list1, predictions_time_list2]
    """
    mean_time = [mean_time_1, mean_time_2]
    number_features = [900, 4378]
    plt.plot( number_features, mean_time, '.', label = 'SVM 1-gram 900-features')
    #plt.plot(mean_time_2, number_features, label = 'SVM 1-gram 2209-features')
    plt.xlabel("Number of features (1-gram)")
    plt.ylabel("Mean prediction time")
    plt.legend()
    plt.show()
    """
    m1 = np.mean(predictions_time_list1)
    st1 = np.std(predictions_time_list1)

    m2 = np.mean(predictions_time_list2)
    st2 = np.std(predictions_time_list2)
    
    m = [m1, m2]
    st = [st1, st2]
    
    fig, ax = plt.subplots()
    bp = ax.boxplot(pred_time_list, showmeans=True)
    for i, line in enumerate(bp['medians']):
        x, y =line.get_xydata()[1]
        text = u'μ={:.4f}\n σ={:.4f}'.format(m[i], st[i])
        ax.annotate(text, xy=(x, y))
        
    #ax.set_title('Linear SVC model: 1-grams with 1642 features and (1,2)-grams with 3004 features',fontsize=10, fontweight='bold', y=1.06)
    ax.set_ylabel('Prediction time [s]',fontsize=14)
    ax.set_xlabel('Number of features',fontsize=14)
    ax.set_xticklabels(['1642', '3004'])
    fig.savefig("svm_1victim_boxplot.pdf", format='pdf')
    plt.show()

#!/usr/bin/env python2
import sys
import os
from os.path import dirname, abspath
import pandas as pd
import numpy as np
import re
import wordninja
import glob
import configuration
from statistics import mean

base_path = (abspath(dirname(__file__)))
logs_path = os.path.join(base_path + '/results_600'+'/multinomial_1_1785/' + '5victims/')

def preprocessing(dataframe):
    """
    This method is used to perform the same preprocessing that is performed offline in the collected dataset.
    It allows to transform the sniffed traffic into BoW, filters and cleans.
    It returns the preprocessed data in form of a dataframe.
    """
    not_to_keep = []
    """
    for webpg in range (len(configuration.webpages_list)):
        div = re.sub('[^A-Za-z0-9]+', ' ', str(configuration.webpages_list[webpg]))
        not_to_keep.append(div)
    for webpg in range (len(not_to_keep)):
        not_to_keep[webpg] = not_to_keep[webpg].split(" ")
    """

    # prepare a list of words that should be droped
    #words_to_delete = [item for sublist in not_to_keep for item in sublist]
    not_to_keep.append('http')
    not_to_keep.append('https')
    not_to_keep.append('ftp')
    not_to_keep.append('www')
    not_to_keep.append('wwwf')
    not_to_keep.append('www2')
    not_to_keep.append('www3')
    not_to_keep.append('co') #
    not_to_keep.append('php')
    not_to_keep.append('html')
    not_to_keep.append('cn')
    not_to_keep.append('ndn')
    not_to_keep.append('PR')
    not_to_keep.append('P1')
    #not_to_keep.append('PR')
    not_to_keep.append('P2')
    not_to_keep.append('site')

    #words_to_delete = list(set(words_to_delete))

    # delete all the special characters from each url
    for row in range (len(dataframe)):
        dataframe['Interest_Request'][row] = re.sub('[^A-Za-z0-9]+', ' ', str(dataframe['Interest_Request'][row]))
        res = dataframe['Interest_Request'][row].split()
        results = []
        for word in res:
            #print("Printing word {}".format(word))
            word_list = wordninja.split(word)
            #print("Printing word list {}".format(word_list))
            results.append(word_list)
        flat_results = [item for sublist in results for item in sublist]

        for index, word in enumerate(flat_results):
            if (len(word) <= 3) or (word.isalpha == False) or (word in not_to_keep):
                if index == 0:
                    dataframe['Interest_Request'][row] = dataframe['Interest_Request'][row].replace(word + ' ', ' ')
                elif index == (len(res) - 1):
                    dataframe['Interest_Request'][row] = dataframe['Interest_Request'][row].replace(' ' + word , ' ')
                else:
                    dataframe['Interest_Request'][row] = dataframe['Interest_Request'][row].replace(' ' + word + ' ', ' ')

    return dataframe

def result_calculation(txt_file, labeled_df):
    df = pd.read_csv(txt_file, delimiter = ',', header=None)
    leg_requests = df.iat[-1, 0].split(':')[-1]
    leg_requests = int(leg_requests)
    #print(leg_requests)
    not_issued_req = leg_requests - len(df)
    #print(not_issued_req)

    df.drop(df.tail(1).index, inplace=True)

    column1 = df[0][0].split(':')[0]
    column2 = df[1][0].split(':')[0]
    column3 = df[2][0].split(':')[0]

    for row in range(len(df)):
        df[0][row] = df[0][row].split(':')[-1]
        df[1][row] = df[1][row].split(':')[-1]
        df[2][row] = df[2][row].split(':')[-1]
    df.columns = [column1, column2, column3]
    #print("Labeled df before: {}".format(labeled_df['Interest_Request']))
    # check if prediction is right
    labeled_df['Interest_Request'] = labeled_df['Interest_Request'].str.strip()
    #labeled_df.to_csv("before_strip.csv")
    #print("Labeled df after: {}".format(labeled_df['Interest_Request']))
    labeled_df.drop_duplicates(subset="Interest_Request", inplace=True)
    #todel = ['  ndn P1 P1', '  ndn P2 P2']
    for i in range(0, len(df)):
        if df['Real request'][i].startswith('  ndn P1 P1'):
            df['Real request'][i] = df['Real request'][i].replace('  ndn P1 P1','')
            #print("P1: {}".format(df['Real request'][i]))
        elif df['Real request'][i].startswith('  ndn P2 P2'):
            #print("P2 before {}".format(df['Real request'][i]))
            df['Real request'][i] = df['Real request'][i].replace('  ndn P2 P2','')
            #print("P2 after: {}".format(df['Real request'][i]))
    df['Real request'] = df['Real request'].str.strip()
    #print("Real requests df {}".format(df['Real request']))
    #df.to_csv('real_df.csv')

    indices = []
    for row in range(len(df)):
        #print(df['Real request'][row])
        index = labeled_df.index[labeled_df['Interest_Request'] == df['Real request'][row]]#.tolist()
        #print(index)
        indices.append(index.tolist())
        
    #print(indices)
    # create a list from list of lists
    indexes_list = []
    for sublist in indices:
        if len(sublist) == 0:
            indexes_list.append(None)
        else:
            for item in sublist:
                indexes_list.append(item)
    #print(indexes_list)
    empty_predictions = 0
    correct_predictions = 0
    incorrect_predictions = 0
    for index, element in enumerate(indexes_list):
        #print("Index {} and element {}".format(index, element))
        #print(element)
        if element==None:
            empty_predictions += 1
        else:
            predicted_label = df[" Attacker's prediction"][index].strip()
            real_label = labeled_df["Label"][element].strip()
            #print(real_label)
            if predicted_label == real_label:
                correct_predictions += 1
            else:
                incorrect_predictions +=1
    #print(empty_predictions)
    total_predictions = leg_requests - empty_predictions - not_issued_req
    print(total_predictions)
    print(correct_predictions + incorrect_predictions + not_issued_req)
    accuracy_score = float(correct_predictions)/float(total_predictions) * 100
    prediction_time_list = df[" Prediction time"].tolist()
    prediction_time_list = [float(item) for item in prediction_time_list]
    prediction_mean_time = mean(prediction_time_list)
    total_prediction_time = prediction_mean_time * float(len(df))

    return df, leg_requests, accuracy_score, prediction_mean_time, total_prediction_time, prediction_time_list

if __name__ == '__main__':
    # load and create a dataframe with labeled dataset
    with open(base_path + "/" + "sim_test_set_lab.csv", "rb") as file:
        labeled_dataframe = pd.read_csv(file)
    labeled_dataframe = labeled_dataframe[labeled_dataframe['Label'].notna()]
    labeled_dataframe = labeled_dataframe.drop("Unnamed: 0", 1)
    labeled_dataframe.columns = ['Interest_Request', 'Label']
    #print(labeled_dataframe)
    labeled_dataframe.index = np.arange(0, len(labeled_dataframe))
    # preprocess dataframe
    prep_dataframe = preprocessing(labeled_dataframe)
    
    #prep_dataframe.to_csv("prepdf.csv")

    # load results for model to be analysed
    os.chdir(logs_path)
    txt_files = glob.glob('*.{}'.format('txt'))
    txt_files.sort()

    # calculate results from each log
    df1, leg_requests_1, accuracy_score1, prediction_mean_time1, total_prediction_time1, prediction_time_list1 = result_calculation(txt_files[0], prep_dataframe)
    df2, leg_requests_2, accuracy_score2, prediction_mean_time2, total_prediction_time2, prediction_time_list2 = result_calculation(txt_files[1], prep_dataframe)
    df3, leg_requests_3, accuracy_score3, prediction_mean_time3, total_prediction_time3, prediction_time_list3  = result_calculation(txt_files[2], prep_dataframe)
    df4, leg_requests_4, accuracy_score4, prediction_mean_time4, total_prediction_time4, prediction_time_list4  = result_calculation(txt_files[3], prep_dataframe)
    #df5, leg_requests_5, accuracy_score5, prediction_mean_time5, total_prediction_time5, prediction_time_list5  = result_calculation(txt_files[4], prep_dataframe)

    accuracy_list = [accuracy_score1 , accuracy_score2 , accuracy_score3, accuracy_score4] # , accuracy_score5]
    total_prediction_list = [total_prediction_time1 , total_prediction_time2, total_prediction_time3, total_prediction_time4] #, total_prediction_time5]
    prediction_mean_time_list = [prediction_mean_time1 , prediction_mean_time2, prediction_mean_time3, prediction_mean_time4] #, prediction_mean_time5]
    pred_list_of_lists = [prediction_time_list1, prediction_time_list2, prediction_time_list3, prediction_time_list4] #, prediction_time_list5]
    pred_times_list = [item for sublist in pred_list_of_lists for item in sublist]

    mean_accuracy_svm = mean(accuracy_list)
    mean_pred_time = mean(prediction_mean_time_list)

    with open("multinomial_1_1785_results.txt", "w") as svm_file:
        svm_file.write("Mean accuracy for 5 iterations is: {}\n".format(mean_accuracy_svm))
        svm_file.write("Mean prediction time for 5 iterations is: {}\n".format(mean_pred_time))
        
        for accuracy in accuracy_list:
            svm_file.write("Accuracy score is: {}\n".format(accuracy))
        for prediction in total_prediction_list:
            svm_file.write("Total prediction time: {}\n".format(prediction))
        for pred_time in prediction_mean_time_list:
            svm_file.write("Mean prediction time per iteration: {}\n".format(pred_time))
        for time in pred_times_list:
            svm_file.write("Prediction time per iteration: {}\n".format(time))
       
